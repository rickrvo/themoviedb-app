# themoviedb-app

**This is a presonal project developed so I can test and improve my Swift skills**

This Project is a work in progress for me to test new concepts, design patterns, etc.

This is the first alpha version I've released so far with the fastest way I could make to create a working version. With that said, here is the TODO list that I'll be doing when I have more time to work on this project:

- Add Watch List feature
- Convert everything to MVVM pattern (right now it's a little bit mixed up)
- Add some cool animations to transations and selections

---

## Actual Project

This is a small movie lister that used The Movie DB (https://www.themoviedb.org) API to list some movies... self explanatory right?

---

## How to run this?

- 1 - Download the repo
- 2 - Go to the "TheMovieDB" folder which contains the Podfile with the terminal
- 3* - type: pod install
- 4 - Then on Finder, open "TheMovieDB.xcworkspace"

3* if by any chance you dont have cocoapods installed and #3 gives out an error. Run this command:

gem install cocoapods

Then you can try step #3 again

---

## Are there 2 different apps in this project?

Yes and No. There is a shceme configuration, just as a proof of concept, on Environment choosing that you can do. This way if you have a Testing environment and a Production environment, you can have 2 different builds at the same time from the same project.

In this case, you can chose to build a Light or a Dark themed version of the app.
