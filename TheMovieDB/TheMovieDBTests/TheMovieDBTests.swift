//
//  TheMovieDBTests.swift
//  TheMovieDBTests
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import XCTest
@testable import TheMovieDB

class TheMovieDBTests: XCTestCase {
    
    var networkManager: NetworkManager?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        networkManager = NetworkManager.shared()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        networkManager = nil
    }

    func testAPI() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        self.testLoadGenres()
        self.testLoadTopRatedList()
    }
    
    func testLoadGenres() {
        networkManager?.getGenres(completion: { [unowned self] (success) in
            if success {
                XCTAssertTrue(self.networkManager?.genres.count ?? 0 > 0)
            }
            else {
                XCTAssertTrue(false)
            }
        })
    }
    
    func testLoadTopRatedList() {
        
        networkManager?.getMoviesList(type: .topRated, completion: { [unowned self] (success) in
            if success {
                XCTAssertTrue(self.networkManager?.topRatedMovies.count ?? 0 > 0)
            }
            else {
                XCTAssertTrue(false)
            }
        })
    }
    
    func testUsingCorrectVersionOfMyFixedAPI() {
        
        networkManager?.getMoviesList(type: .topRated, completion: { (success) in
            if success {
                if self.networkManager?.topRatedMovies.count ?? 0 > 0 {
                    
                    let movie = self.networkManager?.topRatedMovies[0]
                    
                    self.networkManager?.getMovieDetails(for: movie!, completion: { (movieResult) in
                        if movieResult == nil {
                            XCTAssertTrue(false)
                            return
                        }
                        print("reading movie details for: " + (movieResult?.originalTitle ?? "") )
                        if let movieDetail = movieResult {
                            XCTAssertTrue(movieDetail.productionCountries != nil && movieDetail.productionCompanies != nil)
                            return
                        }
                    })
                    XCTAssertTrue(false)
                }
            }
            else {
                XCTAssertTrue(false)
            }
        })
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            
            networkManager?.getMoviesList(type: .popular, completion: { [unowned self] (success) in
                if success {
                    XCTAssertTrue(self.networkManager?.popularMovies.count ?? 0 > 0)
                }
            })
            
        }
    }

}
