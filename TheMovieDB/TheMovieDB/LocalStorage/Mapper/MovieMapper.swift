//
//  MovieMapper.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 17/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import RealmSwift

class MovieMapper {
    

    static func convertRealmObjectToModel(response: MovieModelRealm) -> MovieModel {
        
        let movieModel = MovieModel()
        
        movieModel.description = response.movie_description
        movieModel.favoriteCount = response.favoriteCount.value
        movieModel.itemCount = response.itemCount.value
        movieModel.iso_639_1 = response.iso_639_1
        movieModel.name = response.name
        movieModel.posterPath = response.posterPath
        movieModel.id = response.id.value

        movieModel.adult = response.adult.value
        movieModel.overview = response.overview
        movieModel.popularity = response.popularity.value
        movieModel.backdropPath = response.backdropPath
        movieModel.voteAverage = response.voteAverage.value
        movieModel.originalLanguage = response.originalLanguage
        movieModel.voteCount = response.voteCount.value
        movieModel.genres = response.genres.compactMap({ GenreMapper.convertRealmObjectToModel(response: ($0 as GenreModelRealm)) })

        movieModel.title = response.title
        movieModel.video = response.video.value
        movieModel.releaseDate = response.releaseDate
        movieModel.originalTitle = response.originalTitle
        movieModel.genreIds = response.genreIds.compactMap({ $0 })
        if response.belongsToCollection != nil {
            movieModel.belongsToCollection = MovieMapper.convertRealmObjectToMovieCollection(response: response.belongsToCollection!)
        }
        movieModel.budget = response.budget.value
        movieModel.homepage = response.homepage
        movieModel.imdb_id = response.imdb_id
        movieModel.productionCompanies = response.productionCompanies.compactMap({ MovieMapper.convertRealmObjectToKeywords(response: ($0 as KeywordsModelRealm))})
        movieModel.productionCountries = response.productionCountries.compactMap({ MovieMapper.convertRealmObjectToKeywords(response: ($0 as KeywordsModelRealm))})
        movieModel.revenue = response.revenue.value
        movieModel.runtime = response.runtime.value
        movieModel.spokenLanguages = response.spokenLanguages.compactMap({ MovieMapper.convertRealmObjectToSpokenLanguages(response: ($0 as SpokenLanguagesModelRealm))})
        movieModel.status = response.status
        movieModel.tagline = response.tagline
        
        return movieModel
    }
    
    static func convertRealmObjectToMovieCollection(response: MovieCollectionModelRealm) -> MovieCollection {
        
        var movieCollection = MovieCollection()
        
        movieCollection.id = response.id.value
        movieCollection.name = response.name
        movieCollection.backdrop_path = response.backdrop_path
        movieCollection.poster_path = response.poster_path
        
        return movieCollection
    }
    
    static func convertRealmObjectToKeywords(response: KeywordsModelRealm) -> Keywords {
        
        var keys = Keywords()
        
        keys.id = response.id.value
        keys.name = response.name
        
        return keys
    }
    
    static func convertRealmObjectToSpokenLanguages(response: SpokenLanguagesModelRealm) -> SpokenLanguages {
        
        var langs = SpokenLanguages()
        
        langs.iso_639_1 = response.iso_639_1
        langs.name = response.name
        
        return langs
    }
    
    static func convertModelToRealmObject(response: MovieModel) -> MovieModelRealm {
        
        let movieModel = MovieModelRealm()
        
        movieModel.movie_description = response.description
        movieModel.favoriteCount.value = response.favoriteCount
        movieModel.itemCount.value = response.itemCount
        movieModel.iso_639_1 = response.iso_639_1
        movieModel.name = response.name
        movieModel.posterPath = response.posterPath
        movieModel.id.value = response.id
        
        movieModel.adult.value = response.adult
        movieModel.overview = response.overview
        movieModel.popularity.value = response.popularity
        movieModel.backdropPath = response.backdropPath
        movieModel.voteAverage.value = response.voteAverage
        movieModel.originalLanguage = response.originalLanguage
        movieModel.voteCount.value = response.voteCount
        
        response.genres?.forEach({ (genre) in
            movieModel.genres.append(GenreMapper.convertModelToRealmObject(response: genre))
        })
        
        movieModel.title = response.title
        movieModel.video.value = response.video
        movieModel.releaseDate = response.releaseDate
        movieModel.originalTitle = response.originalTitle
        
        response.genreIds?.forEach({ (id) in
            movieModel.genreIds.append(id)
        })
        
        if response.belongsToCollection != nil {
            movieModel.belongsToCollection = MovieMapper.convertMovieCollectiontoRealObject(response: response.belongsToCollection!)
        }
        movieModel.budget.value = response.budget
        movieModel.homepage = response.homepage
        movieModel.imdb_id = response.imdb_id
        response.productionCompanies?.forEach({ (key) in
            movieModel.productionCompanies.append(MovieMapper.convertKeywordstoRealmObject(response: key))
        })
        response.productionCountries?.forEach({ (key) in
            movieModel.productionCountries.append(MovieMapper.convertKeywordstoRealmObject(response: key))
        })
        movieModel.revenue.value = response.revenue
        movieModel.runtime.value = response.runtime
        response.spokenLanguages?.forEach({ (lang) in
            movieModel.spokenLanguages.append(MovieMapper.convertSpokenLanguagesToRealmObject(response: lang))
        })
        movieModel.status = response.status
        movieModel.tagline = response.tagline
        
        return movieModel
    }
    
    static func convertMovieCollectiontoRealObject(response: MovieCollection) -> MovieCollectionModelRealm {
        
        let movieCollection = MovieCollectionModelRealm()
        
        movieCollection.id.value = response.id
        movieCollection.name = response.name
        movieCollection.backdrop_path = response.backdrop_path
        movieCollection.poster_path = response.poster_path
        
        return movieCollection
    }
    
    
    static func convertKeywordstoRealmObject(response: Keywords) -> KeywordsModelRealm {
        
        let keywords = KeywordsModelRealm()
        
        keywords.id.value = response.id
        keywords.name = response.name
        
        return keywords
    }
    
    static func convertSpokenLanguagesToRealmObject(response: SpokenLanguages) -> SpokenLanguagesModelRealm {
        
        let langs = SpokenLanguagesModelRealm()
        
        langs.iso_639_1 = response.iso_639_1
        langs.name = response.name
        
        return langs
    }
    
    
}
