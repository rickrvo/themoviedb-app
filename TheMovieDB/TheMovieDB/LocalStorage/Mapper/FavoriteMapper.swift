//
//  FavoriteMapper.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 17/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

class FavoriteMapper {
    
    static func convertRealmObjectToModel(response: FavoriteModelRealm) -> FavoriteModel {
        
        var favModel = FavoriteModel()
        
        favModel.id = Int(response.id ?? "0")
        favModel.movie = MovieMapper.convertRealmObjectToModel(response: response.movie ?? MovieModelRealm())
        
        return favModel
    }
    
    static func convertModelToRealmObject(response: FavoriteModel) -> FavoriteModelRealm {
        
        let favObject = FavoriteModelRealm()
        
        favObject.id = "\(response.id ?? 0)"
        favObject.movie = MovieMapper.convertModelToRealmObject(response: response.movie ?? MovieModel())
        
        return favObject
    }
}
