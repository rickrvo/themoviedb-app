//
//  GenreMapper.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 17/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation


class GenreMapper {
    
    static func convertRealmObjectToModel(response: GenreModelRealm) -> GenreModel {
        
        var genreModel = GenreModel()

        genreModel.id = response.genre_id.value
        genreModel.name = response.name
        
        return genreModel
    }
    
    static func convertModelToRealmObject(response: GenreModel) -> GenreModelRealm {
        
        let genreObject = GenreModelRealm()
        
        genreObject.genre_id.value = response.id
        genreObject.name = response.name
        
        return genreObject
    }
}
