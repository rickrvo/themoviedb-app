//
//  WatchListMapper.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

class WatchListMapper {
    
    static func convertRealmObjectToModel(response: WatchListModelRealm) -> WatchListModel {
        
        var watchModel = WatchListModel()
        
        watchModel.id = Int(response.id ?? "0")
        watchModel.movie = MovieMapper.convertRealmObjectToModel(response: response.movie ?? MovieModelRealm())
        
        return watchModel
    }
    
    static func convertModelToRealmObject(response: WatchListModel) -> WatchListModelRealm {
        
        let watchObject = WatchListModelRealm()
        
        watchObject.id = "\(response.id ?? 0)"
        watchObject.movie = MovieMapper.convertModelToRealmObject(response: response.movie ?? MovieModel())
        
        return watchObject
    }
}
