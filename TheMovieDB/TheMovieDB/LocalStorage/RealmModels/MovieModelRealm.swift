//
//  MovieModelRealm.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import RealmSwift

class MovieModelRealm: Object {
    
    @objc dynamic var movie_description: String? = nil
    let favoriteCount = RealmOptional<Int>()
    let itemCount = RealmOptional<Int>()
    @objc dynamic var iso_639_1: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var posterPath: String? = nil
    let id = RealmOptional<Int>()
    
    let adult = RealmOptional<Bool>()
    @objc dynamic var overview: String? = nil
    let popularity = RealmOptional<Double>()
    @objc dynamic var backdropPath: String? = nil
    let voteAverage = RealmOptional<Double>()
    @objc dynamic var originalLanguage: String? = nil
    let voteCount = RealmOptional<Double>()
    let genres = List<GenreModelRealm>()
    
    @objc dynamic var title: String? = nil
    let video = RealmOptional<Bool>()
    @objc dynamic var releaseDate: String? = nil
    @objc dynamic var originalTitle: String? = nil
    let genreIds = List<Int?>()
    
    @objc dynamic var belongsToCollection: MovieCollectionModelRealm? = nil
    let budget = RealmOptional<Int>()
    @objc dynamic var homepage: String? = nil
    @objc dynamic var imdb_id: String? = nil
    let productionCompanies = List<KeywordsModelRealm>()
    let productionCountries = List<KeywordsModelRealm>()
    let revenue = RealmOptional<Int>()
    let runtime = RealmOptional<Int>()
    let spokenLanguages = List<SpokenLanguagesModelRealm>()
    @objc dynamic var status: String? = nil
    @objc dynamic var tagline: String? = nil
}

extension MovieModelRealm {
    enum CodingKeys: String, CodingKey {
        
        case  movie_desciption = "description"
        case  favoriteCount = "favorite_count"
        case  itemCount = "item_count"
        case  iso_639_1
        case  name
        case  posterPath = "poster_path"
        case  id
        
        case  adult
        case  overview
        case  popularity
        case  backdropPath = "backdrop_path"
        case  voteAverage = "vote_average"
        case  originalLanguage = "original_language"
        case  voteCount = "vote_count"
        case  genreIds = "genre_ids"
        
        case  title
        case  video
        case  releaseDate = "release_date"
        case  originalTitle = "original_title"
        case  genres
        
        case  belongsToCollection = "belongs_to_collection"
        case  budget
        case  homepage
        case  imdb_id
        case  productionCompanies = "production_companies"
        case  productionCountries = "production_countries"
        case  revenue
        case  runtime
        case  spokenLanguages = "spoken_languages"
        case  status
        case  tagline
    }
}

class MovieCollectionModelRealm: Object {
    
    let id = RealmOptional<Int>()
    @objc dynamic var name: String? = nil
    @objc dynamic var poster_path: String? = nil
    @objc dynamic var backdrop_path: String? = nil
}

class KeywordsModelRealm: Object {
    
    let id = RealmOptional<Int>()
    @objc dynamic var name: String? = nil
}

class SpokenLanguagesModelRealm: Object {
    
    @objc dynamic var iso_639_1: String? = nil
    @objc dynamic var name: String? = nil
}

class GenreModelRealm: Object {
    let genre_id = RealmOptional<Int>()
    @objc dynamic var name: String? = nil
}
