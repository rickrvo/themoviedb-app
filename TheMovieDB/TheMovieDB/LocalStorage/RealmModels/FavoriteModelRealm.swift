//
//  FavoriteModelRealm.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import RealmSwift

class FavoriteModelRealm: Object {
    
    @objc dynamic var id: String? = nil
    @objc dynamic var movie: MovieModelRealm? = nil
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
