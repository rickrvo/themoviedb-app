//
//  WatchListModelRealm.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import RealmSwift

class WatchListModelRealm: Object {
    
    @objc dynamic var id: String? = nil
    @objc dynamic var movie: MovieModelRealm? = nil
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
