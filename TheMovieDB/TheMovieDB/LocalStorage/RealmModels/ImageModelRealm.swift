//
//  ImageModelRealm.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import RealmSwift

class ImageModelRealm: Object {
    
    @objc dynamic var url: String? = nil
    @objc dynamic var data: Data? = nil
    
    override class func primaryKey() -> String? {
        return "url"
    }
}
