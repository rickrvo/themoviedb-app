//
//  RealmMigrationModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class RealmMigrationModel: NSObject {
    
    var objClassRealm: String!
    var version: String!
    var property: Array<String>!
}
