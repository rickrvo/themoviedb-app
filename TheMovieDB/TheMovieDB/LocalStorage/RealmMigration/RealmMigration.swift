//
//  RealmMigration.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class RealmMigration: NSObject {
    
    var objectsMigration: Array<RealmMigrationModel>!
}


enum EnumRealmMigrationVersion: String {
    case kEmptyMigration = ""
    case kProfileMigration = "ProfileConsultantModelRealm"
    case kConsultantMigration = "ConsultantModelRealm"
    case kCreditCardMigration = "CreditCradModelRealm"
    case kProductMigration = "ProductModelRealm"
    case kFavoriteMigration = "ProductFavoriteRealm"
}
