//
//  RealmMigrationManager.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Realm
import RealmSwift

class RealmMigrationManager: Object {
    
    static func configureMigration() {
        
        if let path = Bundle.main.path(forResource: "RealmMigrationSettings", ofType: "plist") {
            
            let myDict = NSDictionary(contentsOfFile: path)
            if let newVersion = myDict?.object(forKey: "RealmMigrationVersion") as? String {
                
                let config = Realm.Configuration(
                    schemaVersion: UInt64(newVersion)!,
                    migrationBlock: { migration, oldSchemaVersion in
                        print("migration")
                })
                
                Realm.Configuration.defaultConfiguration = config
                
                do {
                    try Realm()
                } catch {
                    print("Realm not started")
                }
            }
        }
    }
}
