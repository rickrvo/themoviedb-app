//
//  MovieEndpoint.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

enum MovieListType {
    case topRated
    case popular
    case nowPlaying
    case upcoming
    case search
}

class MovieNetworkService: NetworkService {
    
    var movieObservable: Observable<NetworkResponse>?
    
    func getMovie(language: String? = nil, movieId: Int, update:@escaping (NetworkResponse, MovieModel?)->Void) {
        
        var params : [String:String] = [:]
        if language != nil {
            params["language"] = language
        }
//        params["append_to_response"] = "videos,images"
        
        params["api_key"] = API_ACCESS_KEY
        
        let routeMovie = Route(target: .movieDetail, targetParams: [":movie_id" : String(movieId) ], params: params, method: .get)
        
        movieObservable = self.singleRequest(route: routeMovie)
        
        movieObservable?
            .subscribe(onNext: { response in
                guard let json = response.data else {
                    return
                }
                if let success = json["status_code"].int {
                    if success == 34 {
                        update(response, nil)
                        return
                    }
                }
                do {
                    let model = try JSONDecoder().decode(MovieModel.self, from: json.rawData())
                    update(response, model)
                } catch let error{
                    print("Network Error: Failed to decode Movie JSON, error: \(error)")
                    update(response, nil)
                }
                
            }, onError: { error in
                print(error)
                
            }).disposed(by: DISPOSABLE_BAG)
    }
    
    func getMovieList(language: String? = nil, listType: MovieListType, page: Int?, update:@escaping (NetworkResponse, [MovieModel]?)->Void) {
        
        var params : [String:String] = [:]
        if language != nil {
            params["language"] = language
        }
        if page != nil {
            params["page"] = String(page!)
        }
        params["api_key"] = API_ACCESS_KEY
        
        let routeMovie: NetworkRoute?
        switch listType {
        case .nowPlaying:
            routeMovie = Route(target: .movieList, targetParams: [":list_type" : "now_playing" ], params: params, method: .get)
        case .popular:
            routeMovie = Route(target: .movieList, targetParams: [":list_type" : "popular"     ], params: params, method: .get)
        case .topRated:
            routeMovie = Route(target: .movieList, targetParams: [":list_type" : "top_rated"   ], params: params, method: .get)
        case .upcoming:
            routeMovie = Route(target: .movieList, targetParams: [":list_type" : "upcoming"    ], params: params, method: .get)
        case .search:
            routeMovie = Route(target: .movieList, targetParams: [":list_type" : "search"      ], params: params, method: .get)
        }
        movieObservable = self.singleRequest(route: routeMovie!)
        
        movieObservable?
            .subscribe(onNext: { response in
                guard let json = response.data else {
                    return
                }
                if let success = json["success"].bool {
                    if success == false {
                        update(response, nil)
                        return
                    }
                }
                do {
                    let model = try JSONDecoder().decode([MovieModel].self, from: json["results"].rawData())
                    update(response, model)
                } catch {
                    print("Network Error: Failed to decode Movie JSON")
                    update(response, nil)
                }
                
            }, onError: { error in
                print(error)
                
            }).disposed(by: DISPOSABLE_BAG)
    }
    
    func getGenres(language: String? = nil, update:@escaping (NetworkResponse ,[GenreModel]?)->Void) {
        
        var params : [String:String] = [:]
        if language != nil {
            params["language"] = language
        }
        params["api_key"] = API_ACCESS_KEY
        
        let routeMovie = Route(target: .genreMovie, params: params , method: .get)
        movieObservable = self.singleRequest(route: routeMovie)
        
        movieObservable?
            .subscribe(onNext: { response in
                guard let json = response.data else {
                    return
                }
                if let success = json["success"].bool {
                    if success == false {
                        update(response, nil)
                        return
                    }
                }
                do {
                    let model = try JSONDecoder().decode([GenreModel].self, from: json["genres"].rawData())
                    update(response, model)
                } catch {
                    print("Network Error: Failed to decode Genre JSON")
                    update(response, nil)
                }
                
            }, onError: { error in
                print(error)
                
            }).disposed(by: DISPOSABLE_BAG)
    }
    
    func getMoviesForGenre(language: String? = nil, genreId: Int, page: Int?, includeAll: Bool = true, includeAdults: Bool = true, update:@escaping (NetworkResponse, [MovieModel]?)->Void) {
        
        var params : [String:String] = [:]
        if language != nil {
            params["language"] = language
        }
        if page != nil {
            params["page"] = String(page!)
        }
        if includeAll {
            params["include_all_movies"] = "1"
        }
        if includeAdults {
            params["include_adult_movies"] = "1"
        }
        params["api_key"] = API_ACCESS_KEY
        
        let routeMovie = Route(target: .moviesGenre, targetParams: [":genre_id" : String(genreId) ], params: params, method: .get)
        
        movieObservable = self.singleRequest(route: routeMovie)
        
        movieObservable?
            .subscribe(onNext: { response in
                guard let json = response.data else {
                    return
                }
                if let success = json["status_code"].int {
                    if success == 34 {
                        update(response, nil)
                        return
                    }
                }
                do {
                    let model = try JSONDecoder().decode([MovieModel].self, from: json["results"].rawData())
                    update(response, model)
                } catch {
                    print("Network Error: Failed to decode MovieForGenre JSON")
                    update(response, nil)
                }
                
            }, onError: { error in
                print(error)
                
            }).disposed(by: DISPOSABLE_BAG)
    }
    
    // MARK: - Search
    
    func searchMovie(language: String? = nil, query: String, page: Int?, year: Int? = nil, primaryReleaseYear: Int? = nil, firstAirDateYear: String? = nil, includeAdults: Bool = true, update:@escaping (NetworkResponse, [MovieModel]?)->Void) {
        
        var params: [String : AnyObject] = ["query": query as AnyObject]
        
        if(page != nil){
            params["page"] = page as AnyObject?
        }
        
        if(language != nil){
            params["language"] = language as AnyObject?
        }
        
        if includeAdults {
            params["include_adult"] = includeAdults as AnyObject?
        }
        
        if let year = year {
            params["year"] = year as AnyObject?
        }
        
        if let primary_release_year = primaryReleaseYear {
            params["primary_release_year"] = primary_release_year as AnyObject?
        }
        
        if let first_air_date_year = firstAirDateYear {
            params["first_air_date_year"] = first_air_date_year as AnyObject?
        }
        
        params["api_key"] = API_ACCESS_KEY as AnyObject
        
        let routeMovie = Route(target: .search, targetParams: [":search_type" : searchType.movie.type ], params: params, method: .get)
        
        movieObservable = self.singleRequest(route: routeMovie)
        
        movieObservable?
            .subscribe(onNext: { response in
                guard let json = response.data else {
                    return
                }
                if let success = json["success"].bool {
                    if success == false {
                        update(response, nil)
                        return
                    }
                }
                do {
                    let model = try JSONDecoder().decode([MovieModel].self, from: json["results"].rawData())
                    update(response, model)
                } catch {
                    print("Network Error: Failed to decode Search JSON")
                    update(response, nil)
                }
                
            }, onError: { error in
                print(error)
                
            }).disposed(by: DISPOSABLE_BAG)
    }
    
}
