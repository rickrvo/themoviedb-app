//
//  APIConstants.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import RxSwift

let DEFAULT_PROTOCOL: NetworkProtocol = .https
let DEFAULT_ENVIRONMENT: Environments = EnvironmentSettings().getCurrentEnviroment()

let DEFAULT_NETWORK_TIMEOUT: TimeInterval = 20
let DEFAULT_NETWORK_REQUEST_INTERVAL: TimeInterval = 15
let API_ACCESS_KEY = "d781b8ca7663031d60a34ede6eb731e2"

let DISPOSABLE_BAG: DisposeBag = DisposeBag()


enum Endpoint:String, NetworkEndpoint {
    
    var endpoint: String {
        return self.rawValue
    }
    
    case movieList = "movie/:list_type"
    case movieDetail = "movie/:movie_id"
    case genreMovie = "genre/movie/list"
    case genreTv = "genre/tv/list"
    case moviesGenre = "genre/:genre_id/movies"
    case search = "search/:search_type"
    
}

enum searchType: String {
    
    var type: String {
        return self.rawValue
    }
    
    case movie = "movie"
    case tv = "tv"
    case people = "person"
    case keywords = "keyword"
    case companies = "company"
}

