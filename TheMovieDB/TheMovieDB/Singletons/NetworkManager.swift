//
//  NetworkManager.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

class NetworkManager {
    
    var genres : [GenreModel]
    var genresList : [Int : [MovieModel]]
    
    var topRatedMovies : [MovieModel]
    var popularMovies : [MovieModel]
    var nowPlayingMovies : [MovieModel]
    var upcomingMovies : [MovieModel]
    var searchResults : [MovieModel]
    
    var latestSearch: String
    
    var currentGenreListPage : [Int : Int]
    var currentTopRatedPage : Int
    var currentPopularPage : Int
    var currentNowPlayingPage : Int
    var currentUpcomingPage : Int
    var currentResultsPage : Int
    
    var totalGenreListPage : [Int : Int]
    var totalTopRatedPage : Int
    var totalPopularPage : Int
    var totalNowPlayingPage : Int
    var totalUpcomingPage : Int
    var totalSearchPage : Int
    
    let language = Locale.current.languageCode
    
    let imageBaseURL = "http://image.tmdb.org/t/p/"
    
    let imageSize185 = "w185/"
    let imageSize300 = "w300/"
    let imageSize780 = "w780/"
    
    private static var sharedNetworkManager: NetworkManager = {
        let movieManager = NetworkManager()
        return movieManager
    }()
    
    let movieService = MovieNetworkService()
    
    private init() {
        self.genres = []
        self.genresList = [:]
        self.topRatedMovies = []
        self.popularMovies = []
        self.nowPlayingMovies = []
        self.upcomingMovies = []
        self.searchResults = []
        
        self.currentGenreListPage = [:]
        self.currentPopularPage = 0
        self.currentTopRatedPage = 0
        self.currentNowPlayingPage = 0
        self.currentUpcomingPage = 0
        self.currentResultsPage = 0
        
        self.totalGenreListPage = [:]
        self.totalPopularPage = Int.max
        self.totalTopRatedPage = Int.max
        self.totalNowPlayingPage = Int.max
        self.totalUpcomingPage = Int.max
        self.totalSearchPage = Int.max
        
        self.latestSearch = ""
    }
    
    // Accessors
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    // MARK: - GENRES
    
    func getGenreNameByID(id: Int) -> String {
        
        if self.genres.count > 0 {
            let index = self.genres.firstIndex { (genre) -> Bool in
                genre.id == id
            }
            if index != nil {
                return self.genres[index!].name ?? ""
            }
        }
        return ""
    }
    
    func getAllGenreNamesByIDs(genreIDs: [Int]?) -> String {
        
        var result = ""
        for g in genreIDs ?? [] {
            result += self.getGenreNameByID(id: g) + " & "
        }
        if result != "" {
            result = String(result.dropLast(3))
        }
        return result
    }
    
    public typealias genresType = (id: Int?, name: String?)
    func getAllGenreNames(genres: [genresType]?) -> String {
        
        var result = ""
        for g in genres ?? [] {
            result += (g.name ?? "") + " & "
        }
        if result != "" {
            result = String(result.dropLast(3))
        }
        return result
    }
    
    func getGenres(completion: ((Bool)->())?) {
        movieService.getGenres(language: language) { (result, data) in
            if result.code != 200 || data == nil {
                completion?(false)
                return
            }
            print("reading genres")
            if let genre = data{
                genre.forEach{
                    print($0.name ?? "")
                    let g0 = $0
                    if (!self.genres.contains(where: { (g) -> Bool in
                        g.id == g0.id
                    })) {
                        self.genres.append(g0)
                    }
                }
            }
            completion?(true)
        }
    }
    
    // MARK: - MOVIES
    
    func getMovieDetails(for movie: MovieModel, completion: ((MovieModel?)->())?) {
        
        if movie.id == nil {
            completion?(nil)
            return
        }
        
        movieService.getMovie(movieId: movie.id!, update: { (result, movies) in
            
            if result.code != 200 || movies == nil {
                completion?(nil)
                return
            }

            print("reading movie id: \(String(describing: movie.id)) name: \(String(describing: movie.name))")
            if let movie = movies {
                completion?(movie)
                return
            }
            completion?(nil)
        })
    }
    
    func getMovieGenres(for genre: GenreModel, completion: ((Bool)->())?) {
        if genre.id == nil {
            completion?(false)
            return
        }
        if self.currentGenreListPage[genre.id!] == nil {
            self.currentGenreListPage[genre.id!] = 1
        } else {
            self.currentGenreListPage[genre.id!] = self.currentGenreListPage[genre.id!]! + 1
        }
        if self.totalGenreListPage[genre.id!] == nil {
            self.totalGenreListPage[genre.id!] = Int.max
        }
        if self.currentGenreListPage[genre.id!]!  <= self.totalGenreListPage[genre.id!]!  {
            
            movieService.getMoviesForGenre(language: language, genreId: genre.id!, page: self.currentGenreListPage[genre.id!], includeAll: true, includeAdults: true) { [unowned self] (result, movies) in
                
                if result.code != 200 || movies == nil {
                    completion?(false)
                    self.currentGenreListPage[genre.id!] = self.currentGenreListPage[genre.id!]! - 1
                    return
                }
                if let total = result.data?["total_pages"] {
                    self.totalGenreListPage[genre.id!] = total.int ?? 0
                }
                print("reading movies for genre: " + (genre.name ?? "") )
                if let movie = movies {
                    movie.forEach{
                        print($0.title ?? "")
                        let m0 = $0
                        if (self.genresList[genre.id!] == nil) {
                            self.genresList[genre.id!] = [m0]
                        } else {
                            if (!self.genresList[genre.id!]!.contains(where: { (m) -> Bool in
                                m.id == m0.id
                            })) {
                                self.genresList[genre.id!]?.append(m0)
                            }
                        }
                    }
                }
                completion?(true)
            }
        } else {
            completion?(false)
        }
    }
    
    func getMoviesList(type: MovieListType, completion: ((Bool)->())?) {
        switch type {
        case .topRated:
            self.currentTopRatedPage += 1
            
            if self.currentTopRatedPage <= self.totalTopRatedPage {
                
                movieService.getMovieList(language: language, listType: .topRated, page: self.currentTopRatedPage) { [unowned self] (result, movies) in
                    
                    if result.code != 200 || movies == nil {
                        completion?(false)
                        self.currentTopRatedPage -= 1
                        return
                    }
                    if let total = result.data?["total_pages"] {
                        self.totalTopRatedPage = total.int ?? 0
                    }
                    print("reading movies for top rated")
                    if let movie = movies {
                        movie.forEach{
                            print($0.title ?? "")
                            let m0 = $0
                            if (!self.topRatedMovies.contains(where: { (m) -> Bool in
                                m.id == m0.id
                            })) {
                                self.topRatedMovies.append(m0)
                            }
                        }
                    }
                    completion?(true)
                }
            } else {
                completion?(false)
            }
        case .popular:
            self.currentPopularPage += 1
            if self.currentPopularPage <= self.totalPopularPage {
                movieService.getMovieList(language: language, listType: .popular, page: self.currentPopularPage) { [unowned self] (result, movies) in
                    
                    if result.code != 200 || movies == nil {
                        completion?(false)
                        self.currentPopularPage -= 1
                        return
                    }
                    if let total = result.data?["total_pages"] {
                        self.totalPopularPage = total.int ?? 0
                    }
                    print("reading movies for popular")
                    if let movie = movies {
                        movie.forEach{
                            print($0.title ?? "")
                            let m0 = $0
                            if (!self.popularMovies.contains(where: { (m) -> Bool in
                                m.id == m0.id
                            })) {
                                self.popularMovies.append(m0)
                            }
                        }
                    }
                    completion?(true)
                }
            } else {
                completion?(false)
            }
        case .nowPlaying:
            self.currentNowPlayingPage += 1
            if self.currentNowPlayingPage <= self.totalNowPlayingPage {
                movieService.getMovieList(language: language, listType: .nowPlaying, page: self.currentNowPlayingPage) { [unowned self] (result, movies) in
                    
                    if result.code != 200 || movies == nil {
                        completion?(false)
                        self.currentNowPlayingPage -= 1
                        return
                    }
                    if let total = result.data?["total_pages"] {
                        self.totalNowPlayingPage = total.int ?? 0
                    }
                    print("reading movies for nowPlaying")
                    if let movie = movies {
                        movie.forEach{
                            print($0.title ?? "")
                            let m0 = $0
                            if (!self.nowPlayingMovies.contains(where: { (m) -> Bool in
                                m.id == m0.id
                            })) {
                                self.nowPlayingMovies.append(m0)
                            }
                        }
                    }
                    completion?(true)
                }
            } else {
                completion?(false)
            }
        case .upcoming:
            self.currentUpcomingPage += 1
            if self.currentUpcomingPage <= self.totalUpcomingPage {
                movieService.getMovieList(language: language, listType: .upcoming, page: self.currentUpcomingPage) { [unowned self] (result, movies) in
                    
                    if result.code != 200 || movies == nil {
                        completion?(false)
                        self.currentUpcomingPage -= 1
                        return
                    }
                    if let total = result.data?["total_pages"] {
                        self.totalUpcomingPage = total.int ?? 0
                    }
                    print("reading movies for upcoming")
                    if let movie = movies {
                        movie.forEach{
                            print($0.title ?? "")
                            let m0 = $0
                            if (!self.upcomingMovies.contains(where: { (m) -> Bool in
                                m.id == m0.id
                            })) {
                                self.upcomingMovies.append(m0)
                            }
                        }
                    }
                    completion?(true)
                }
            } else {
                completion?(false)
            }
            
        default:
            completion?(false)
        }
    }
    
    // MARK:- SEARCH
    
    func searchMovies(search: String, completion: ((Bool)->())?) {
        if self.latestSearch != search { // if it's not the same search we will clear previous results
            self.latestSearch = search
            self.currentResultsPage = 0
            self.totalSearchPage = Int.max
            self.searchResults = []
        }
        self.currentResultsPage += 1
        if self.currentResultsPage <= self.totalSearchPage {
            
            movieService.searchMovie(query: search, page: self.currentResultsPage) { (result, movies) in
                
                if result.code != 200 || movies == nil {
                    completion?(false)
                    self.currentResultsPage -= 1
                    return
                }
                if let total = result.data?["total_pages"] {
                    self.totalSearchPage = total.int ?? 0
                }
                print("reading movies for searching")
                if let movie = movies {
                    movie.forEach{
                        print($0.title ?? "")
                        let m0 = $0
                        if (!self.searchResults.contains(where: { (m) -> Bool in
                            m.id == m0.id
                        })) {
                            self.searchResults.append(m0)
                        }
                    }
                }
                completion?(true)
            }
            
        } else {
            completion?(false)
        }
    }
}
