//
//  EnvironmentModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class Environments: NSObject {
    var name: String!
    var backgroundColor: String!
    var textColor: String!
}
