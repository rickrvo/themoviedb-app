//
//  EnvironmentSettings.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import UIKit

class EnvironmentSettings: NSObject {
    
    static let sharedInstance = EnvironmentSettings()
    
    var environment: Environments!
    var chosenEnvironment: String!
    
    override init() {
        super.init()
        self.loadItems()
    }
    
    func loadItems() {
        self.chosenEnvironment = self.getEnvironmentSettingsItem(item: "chosenEnvironment") as? String
        self.environment = self.getCurrentEnviroment()
    }
    
    func getEnvironmentSettingsItem(item :String) -> AnyObject
    {
        let filePath = Bundle.main.path(forResource: "Environments", ofType: "plist")!
        let stylesheet : NSDictionary! = NSDictionary(contentsOfFile:filePath)
        
        return stylesheet.object(forKey: item)! as AnyObject
    }
    
    func getCurrentEnviroment() -> Environments {
        let envs = self.getEnvironmentSettingsItem(item: "environments") as! NSArray
        var currentEnvironment: Environments?
        for env in envs {
            let environment = Environments()
            
            if let envDictionary = env as? NSDictionary {
                environment.name = envDictionary.object(forKey:"name") as? String
                
                if (environment.name != self.chosenEnvironment){
                    continue
                }
                
                environment.name = envDictionary.object(forKey:"name") as? String ?? ""
                environment.backgroundColor = envDictionary.object(forKey:"backgroundColor") as? String ?? ""
                environment.textColor = envDictionary.object(forKey:"textColor") as? String ?? ""
                
                currentEnvironment = environment
            }
            break
        }
        
        return currentEnvironment!
    }
}
