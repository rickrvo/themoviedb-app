//
//  SideMenuViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import UIKit

class SideMenuViewController: UIViewController {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var btGenres: UIButton!
    @IBOutlet weak var btWatchList: UIButton!
    @IBOutlet weak var watchListLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.watchListLabel.textColor = UIColor(hex: env.backgroundColor)
            self.genresLabel.textColor = UIColor(hex: env.backgroundColor)
            self.btGenres.setImage(UIImage(named: "Genre"), for: .normal)
            self.btWatchList.setImage(UIImage(named: "WatchListWhite"), for: .normal)
            self.genresLabel.textColor = UIColor.white
            self.watchListLabel.textColor = UIColor.white
        }
    }
    
    @IBAction func genresTap(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("SideMenu-Genres"), object: nil)
    }
    
    @IBAction func watchListTap(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("SideMenu-WatchList"), object: nil)
    }
}
