//
//  MovieListViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {
    
    var genre:GenreModel? = nil
    var listType:MovieListType? = nil
    var searchTerm:String? = nil
    
    var networkManager: NetworkManager?
    
    var loadingView = LoadingView()
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.networkManager = NetworkManager.shared()
        
        if genre != nil {
            self.titleLabel.text = genre?.name
        } else {
            switch listType {
            case .topRated?:
                self.titleLabel.text = "TOP_RATED".localized
            case .popular?:
                self.titleLabel.text = "MOST_POPULAR".localized
            case .nowPlaying?:
                self.titleLabel.text = "NOW_PLAYING".localized
            case .upcoming?:
                self.titleLabel.text = "UPCOMING".localized
            case .search?:
                self.titleLabel.text = searchTerm
            default:
                print("Something wrong with the listType")
                self.titleLabel.text = ""
            }
        }
        
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.navView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.backButton.setImage(UIImage(named: "backWhite"), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            return .lightContent
        }
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if genre != nil {
            self.loadMoviesGenres()
        } else {
            self.loadMoviesList()
        }
    }
    
    func registerCells() {
        let movieViewCell = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        self.collectionView.register(movieViewCell, forCellWithReuseIdentifier: "MovieCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func loadMoviesGenres() {
        self.loadingView.showOverlayTransparent(over: self.view)
        
        networkManager?.getMovieGenres(for: self.genre!, completion: { [unowned self] (success) in
            if success {
                self.collectionView.reloadData()
            }
            self.loadingView.hideOverlayView()
        })
    }
    
    func loadMoviesList() {
        self.loadingView.showOverlayTransparent(over: self.view)
        
        switch listType {
        case .topRated?:
            networkManager?.getMoviesList(type: .topRated, completion: { [unowned self] (success) in
                if success {
                    self.collectionView.reloadData()
                }
                self.loadingView.hideOverlayView()
            })
        case .popular?:
            networkManager?.getMoviesList(type: .popular, completion: { [unowned self] (success) in
                if success {
                    self.collectionView.reloadData()
                }
                self.loadingView.hideOverlayView()
            })
        case .nowPlaying?:
            networkManager?.getMoviesList(type: .nowPlaying, completion: { [unowned self] (success) in
                if success {
                    self.collectionView.reloadData()
                }
                self.loadingView.hideOverlayView()
            })
        case .upcoming?:
            networkManager?.getMoviesList(type: .upcoming, completion: { [unowned self] (success) in
                if success {
                    self.collectionView.reloadData()
                }
                self.loadingView.hideOverlayView()
            })
        case .search?:
            networkManager?.searchMovies(search: searchTerm!, completion: { [unowned self] (success) in
                if success {
                    self.collectionView.reloadData()
                }
                self.loadingView.hideOverlayView()
            })
        default:
            print("Something wrong with the listType")
            self.titleLabel.text = ""
            self.loadingView.hideOverlayView()
        }
    }
    
    
    // MARK: - Navigation
    var tapped:Bool = false
    @IBAction func backButtonTap(_ sender: Any) {
        if tapped {
            return
        }
        tapped = true
        self.navigationController?.popViewController(animated: true)
        tapped = false
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    }
}

extension MovieListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if genre != nil {
            return self.networkManager?.genresList[(self.genre?.id)!]?.count ?? 0
            
        } else {
            
            switch listType {
            case .topRated?:
                return networkManager?.topRatedMovies.count ?? 0
            case .popular?:
                return networkManager?.popularMovies.count ?? 0
            case .nowPlaying?:
                return networkManager?.nowPlayingMovies.count ?? 0
            case .upcoming?:
                return networkManager?.upcomingMovies.count ?? 0
            case .search?:
                return networkManager?.searchResults.count ?? 0
            default:
                print("Something wrong with the listType")
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
        
        if genre != nil {
            if let movie:MovieModel = self.networkManager?.genresList[(self.genre?.id)!]?[indexPath.item] {
                cell.setup(content: movie)
            }
        } else {
            switch listType {
            case .topRated?:
                if let movie:MovieModel = self.networkManager?.topRatedMovies[indexPath.item] {
                    cell.setup(content: movie)
                }
            case .popular?:
                if let movie:MovieModel = self.networkManager?.popularMovies[indexPath.item] {
                    cell.setup(content: movie)
                }
            case .nowPlaying?:
                if let movie:MovieModel = self.networkManager?.nowPlayingMovies[indexPath.item] {
                    cell.setup(content: movie)
                }
            case .upcoming?:
                if let movie:MovieModel = self.networkManager?.upcomingMovies[indexPath.item] {
                    cell.setup(content: movie)
                }
            case .search?:
                if let movie:MovieModel = self.networkManager?.searchResults[indexPath.item] {
                    cell.setup(content: movie)
                }
            default:
                print("Something wrong with the listType")
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if tapped {
            return
        }
        tapped = true
        
        var selectedMovie: MovieModel?
        
        if genre != nil {
            
            if let movie:MovieModel = self.networkManager?.genresList[(self.genre?.id)!]?[indexPath.item] {
                selectedMovie = movie
            }
            
        } else {
            
            if let type = listType {
                switch type {
                case .topRated:
                    selectedMovie = networkManager?.topRatedMovies[indexPath.item]
                case .popular:
                    selectedMovie = networkManager?.popularMovies[indexPath.item]
                case .nowPlaying:
                    selectedMovie = networkManager?.nowPlayingMovies[indexPath.item]
                case .upcoming:
                    selectedMovie = networkManager?.upcomingMovies[indexPath.item]
                case .search:
                    selectedMovie = networkManager?.searchResults[indexPath.item]
                }
            }
        }
        
        if let movie = selectedMovie {
            
            let viewController:MovieDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
            viewController.movie = movie
            self.navigationController?.show(viewController, sender: self)
        }
        
        tapped = false
    }
    
    // Make Cells align in the center
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let cellWidth : CGFloat = 108.0
        
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        
        return UIEdgeInsets(top: 15, left: edgeInsets, bottom: 0, right: edgeInsets)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if genre != nil {
            
            if indexPath.item >= ((networkManager?.genresList[genre!.id!]!.count ?? 0) - 1) {
                
                self.loadingView.showOverlayTransparent(over: self.view)
                networkManager?.getMovieGenres(for: genre!, completion: { [unowned self] (success) in
                    if success {
                        self.collectionView.reloadData()
                    }
                    self.loadingView.hideOverlayView()
                })
            }
            
        } else {
            
            switch listType {
            case .topRated?:
                if indexPath.item >= ((networkManager?.topRatedMovies.count ?? 0) - 1) {
                    self.loadingView.showOverlayTransparent(over: self.view)
                    networkManager?.getMoviesList(type: .topRated, completion: { [unowned self] (success) in
                        if success {
                            self.collectionView.reloadData()
                        }
                        self.loadingView.hideOverlayView()
                    })
                }
            case .popular?:
                if indexPath.item >= ((networkManager?.popularMovies.count ?? 0) - 1) {
                    self.loadingView.showOverlayTransparent(over: self.view)
                    networkManager?.getMoviesList(type: .popular, completion: { [unowned self] (success) in
                        if success {
                            self.collectionView.reloadData()
                        }
                        self.loadingView.hideOverlayView()
                    })
                }
            case .nowPlaying?:
                if indexPath.item >= ((networkManager?.nowPlayingMovies.count ?? 0) - 1) {
                    self.loadingView.showOverlayTransparent(over: self.view)
                    networkManager?.getMoviesList(type: .nowPlaying, completion: { [unowned self] (success) in
                        if success {
                            self.collectionView.reloadData()
                        }
                        self.loadingView.hideOverlayView()
                    })
                }
            case .upcoming?:
                if indexPath.item >= ((networkManager?.upcomingMovies.count ?? 0) - 1) {
                    self.loadingView.showOverlayTransparent(over: self.view)
                    networkManager?.getMoviesList(type: .upcoming, completion: { [unowned self] (success) in
                        if success {
                            self.collectionView.reloadData()
                        }
                        self.loadingView.hideOverlayView()
                    })
                }
            case .search?:
                if indexPath.item >= ((networkManager?.searchResults.count ?? 0) - 1) {
                    self.loadingView.showOverlayTransparent(over: self.view)
                    networkManager?.searchMovies(search: searchTerm!, completion: { [unowned self] (success) in
                        if success {
                            self.collectionView.reloadData()
                        }
                        self.loadingView.hideOverlayView()
                    })
                }
            default:
                print("Something wrong with the listType")
            }
        }
    }
    
}
