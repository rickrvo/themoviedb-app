//
//  GenresViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class GenresViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var networkManager : NetworkManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.networkManager = NetworkManager.shared()
        
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.navView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.backButton.setImage(UIImage(named: "backWhite"), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            return .lightContent
        }
        return .default
    }
    
    func registerCells() {
        self.tableView.register(UINib(nibName: "GenreTableViewCell", bundle: nil), forCellReuseIdentifier: "GenreCell")
    }
    
    
    // MARK: - Navigation
    
    var tapped: Bool = false
    @IBAction func backButtonTap(_ sender: Any) {
        if tapped {
            return
        }
        tapped = true
        let nav = self.navigationController
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().popFromRight(), forKey: nil)
            nav?.popViewController(animated: false)
        }
        tapped = false
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    }
    
}

extension GenresViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.networkManager?.genres.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GenreCell") as! GenreTableViewCell
        cell.titleLabel.text = self.networkManager?.genres[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tapped {
            return
        }
        tapped = true
        
        let genre:GenreModel = (networkManager?.genres[indexPath.row])!
        let viewController:MovieListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
        viewController.genre = genre
        self.navigationController?.show(viewController, sender: self)
        
        tapped = false
    }
    
}
