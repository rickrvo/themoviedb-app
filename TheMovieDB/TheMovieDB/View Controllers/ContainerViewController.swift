//
//  ContainerViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sideMenuConstraint: NSLayoutConstraint!
    var sideMenuOpen = false
    var tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(toggleSideMenu), name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    
    @objc func toggleSideMenu() {
        
        if sideMenuOpen {
            
            sideMenuOpen = false
            self.sideMenuConstraint.constant = -240
            
            mainView.removeGestureRecognizer(tap)
            
        } else {
            
            sideMenuOpen = true
            self.sideMenuConstraint.constant = 0
            
            mainView.addGestureRecognizer(tap)
            
            mainView.isUserInteractionEnabled = true
            
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.toggleSideMenu()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
