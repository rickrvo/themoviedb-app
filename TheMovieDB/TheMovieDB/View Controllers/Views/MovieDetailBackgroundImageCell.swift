//
//  MovieDetailBackgroundImageCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class MovieDetailBackgroundImageCell: UITableViewCell {
    
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var imageGradient: GradientView!
    
    var networkManager : NetworkManager?
    var movie: MovieModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.networkManager = NetworkManager.shared()
        
        self.setupUI()
        
    }
    
    func setupUI() {
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.imageGradient.bottomColor = UIColor(hex: env.backgroundColor)
        }
    }
    
    func setup(content: MovieModel?) {
        
        guard let c = content else { return }
        self.movie = c
        
        if let path = self.movie?.backdropPath {
            imageBackground.getImage(urlString: networkManager!.imageBaseURL + networkManager!.imageSize300 + path)
        }
        
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
