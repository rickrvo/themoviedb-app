//
//  MovieCollectionViewCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit
import Cosmos

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageThumb: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var buttonFavorite: UIButton!
    @IBOutlet weak var buttonWatchList: UIButton!
    
    var movie:MovieModel?
    var networkManager : NetworkManager?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        networkManager = NetworkManager.shared()
        
        setupUI()
    }
    
    func setupUI() {
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.labelTitle.textColor = UIColor(hex: env.textColor)
            
            self.buttonFavorite.setImage(UIImage(named: "FavoriteSelectedWhite"), for: .selected)
            self.buttonFavorite.setImage(UIImage(named: "FavoriteUnselectedWhite"), for: .normal)
            self.buttonWatchList.setImage(UIImage(named: "WatchListWhite"), for: .selected)
            self.buttonWatchList.setImage(UIImage(named: "WatchListAddWhite"), for: .normal)
        }
        
        if let m = movie {
            let query : NSPredicate = NSPredicate(format: "id == '\(m.id ?? 0)'")
            if let fav = RealmDataManager.fetchDataWith(query: query, entity: FavoriteModelRealm.self) {
                if fav.count > 0 {
                    buttonFavorite.isSelected = true
                } else {
                    buttonFavorite.isSelected = false
                }
            }
            if let watch = RealmDataManager.fetchDataWith(query: query, entity: WatchListModelRealm.self) {
                if watch.count > 0 {
                    buttonWatchList.isSelected = true
                } else {
                    buttonWatchList.isSelected = false
                }
            }
        }
    }
    
    func setup(content: MovieModel?) {
        
        guard let c = content else { return }
        self.movie = c
        
        labelTitle.text = self.movie?.originalTitle
        starRating.text = String(self.movie?.voteAverage ?? 0)
        starRating.rating = (self.movie?.voteAverage ?? 0.0) / 2
        if let path = self.movie?.posterPath {
            self.imageThumb.getImage(urlString: networkManager!.imageBaseURL + networkManager!.imageSize185 + path)
        }
        
        setupUI()
    }
    
    
    @IBAction func favoriteTap(_ sender: Any) {
        
        if buttonFavorite.isSelected {
            buttonFavorite.isSelected = false
            
            let query : NSPredicate = NSPredicate(format: "id == '\(movie?.id ?? 0)'")
            RealmDataManager.deleteWith(query: query, entity: FavoriteModelRealm.self)
        } else {
            buttonFavorite.isSelected = true
            
            let fav = FavoriteModel(id: movie?.id, movie: movie)
            let favRealm = FavoriteMapper.convertModelToRealmObject(response: fav)
            RealmDataManager.addItem(entity: favRealm, type: FavoriteModelRealm.self)
        }
        NotificationCenter.default.post(name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
    @IBAction func watchListTap(_ sender: Any) {
        
        if buttonWatchList.isSelected {
            buttonWatchList.isSelected = false
            
            let query : NSPredicate = NSPredicate(format: "id == '\(movie?.id ?? 0)'")
            RealmDataManager.deleteWith(query: query, entity: WatchListModelRealm.self)
        } else {
            buttonWatchList.isSelected = true
            
            let watch = WatchListModel(id: movie?.id, movie: movie)
            let watchRealm = WatchListMapper.convertModelToRealmObject(response: watch)
            RealmDataManager.addItem(entity: watchRealm, type: WatchListModelRealm.self)
        }
        NotificationCenter.default.post(name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
}
