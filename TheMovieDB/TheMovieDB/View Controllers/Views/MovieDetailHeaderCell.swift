//
//  MovieDetailHeaderCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Cosmos
import UIKit

protocol MovieShareDelegate {
    func didTapShare()
}

class MovieDetailHeaderCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageThumbMovie: UIImageView!
    @IBOutlet weak var titleOriginalLabel: UILabel!
    @IBOutlet weak var tagLineLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var ratingCosmos: CosmosView!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var buttonWatchList: UIButton!
    @IBOutlet weak var buttonFavorite: UIButton!
    
    var networkManager : NetworkManager?
    var delegate:MovieShareDelegate?
    var movie: MovieModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.networkManager = NetworkManager.shared()
        
        self.setupUI()
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.bgView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.titleOriginalLabel.textColor = UIColor(hex: env.textColor)
            self.tagLineLabel.textColor = UIColor(hex: env.textColor)
            self.statusLabel.textColor = UIColor(hex: env.textColor)
            self.shareButton.setImage(UIImage(named: "ShareWhite"), for: .normal)
            self.buttonFavorite.setImage(UIImage(named: "FavoriteSelectedWhite"), for: .selected)
            self.buttonFavorite.setImage(UIImage(named: "FavoriteUnselectedWhite"), for: .normal)
            self.buttonWatchList.setImage(UIImage(named: "WatchListWhite"), for: .selected)
            self.buttonWatchList.setImage(UIImage(named: "WatchListAddWhite"), for: .normal)
        }
        
    }
    
    func setupUI() {
        
        if let m = movie {
            let query : NSPredicate = NSPredicate(format: "id == '\(m.id ?? 0)'")
            if let fav = RealmDataManager.fetchDataWith(query: query, entity: FavoriteModelRealm.self) {
                if fav.count > 0 {
                    buttonFavorite.isSelected = true
                } else {
                    buttonFavorite.isSelected = false
                }
            }
            if let watch = RealmDataManager.fetchDataWith(query: query, entity: WatchListModelRealm.self) {
                if watch.count > 0 {
                    buttonWatchList.isSelected = true
                } else {
                    buttonWatchList.isSelected = false
                }
            }
        }
    }
    
    func setup(content: MovieModel?) {
        
        guard let c = content else { return }
        self.movie = c
        
        if let path = self.movie?.posterPath {
            imageThumbMovie.getImage(urlString: networkManager!.imageBaseURL + networkManager!.imageSize185 + path)
        }
        ratingCosmos.text = String(self.movie?.voteAverage ?? 0) + " / 10"
        titleOriginalLabel.text = self.movie?.originalTitle
        tagLineLabel.text = self.movie?.tagline
        statusLabel.text = self.movie?.status
        ratingCosmos.rating = (self.movie?.voteAverage ?? 0.0) / 2
        voteCountLabel.text = String(format: "%.0f", self.movie?.voteCount ?? 0) + "X_VOTES".localized
        
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func shareButtonTap(_ sender: Any) {
        self.delegate?.didTapShare()
    }
    
    @IBAction func watchListTap(_ sender: Any) {
        
        if buttonWatchList.isSelected {
            buttonWatchList.isSelected = false
            
            let query : NSPredicate = NSPredicate(format: "id == '\(movie?.id ?? 0)'")
            RealmDataManager.deleteWith(query: query, entity: WatchListModelRealm.self)
        } else {
            buttonWatchList.isSelected = true
            
            let watch = WatchListModel(id: movie?.id, movie: movie)
            let watchRealm = WatchListMapper.convertModelToRealmObject(response: watch)
            RealmDataManager.addItem(entity: watchRealm, type: WatchListModelRealm.self)
        }
        NotificationCenter.default.post(name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
    @IBAction func favoriteTap(_ sender: Any) {
        
        if buttonFavorite.isSelected {
            buttonFavorite.isSelected = false
            
            let query : NSPredicate = NSPredicate(format: "id == '\(movie?.id ?? 0)'")
            RealmDataManager.deleteWith(query: query, entity: FavoriteModelRealm.self)
        } else {
            buttonFavorite.isSelected = true
            
            let fav = FavoriteModel(id: movie?.id, movie: movie)
            let favRealm = FavoriteMapper.convertModelToRealmObject(response: fav)
            RealmDataManager.addItem(entity: favRealm, type: FavoriteModelRealm.self)
        }
        NotificationCenter.default.post(name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
}
