//
//  MovieDetailMultiLineCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class MovieDetailMultiLineCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    func setupUI() {
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.keyLabel.textColor = UIColor(hex: env.textColor)
            self.valueLabel.textColor = UIColor(hex: env.textColor)
        }
    }
    
    func setup(key: String?, value: String?) {
        
        keyLabel.text = key
        valueLabel.text = value
        
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
