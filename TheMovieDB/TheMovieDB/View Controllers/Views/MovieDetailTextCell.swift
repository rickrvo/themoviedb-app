//
//  MovieDetailTextCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class MovieDetailTextCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textInfoLabel: UILabel!
    
    var movie:MovieModel?
    var networkManager : NetworkManager?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        networkManager = NetworkManager.shared()
        
        setupUI()
    }
    
    func setupUI() {
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.titleLabel.textColor = UIColor(hex: env.textColor)
            self.textInfoLabel.textColor = UIColor(hex: env.textColor)
        }
    }
    
    func setup(content: MovieModel?) {
        
        guard let c = content else { return }
        self.movie = c
        
        titleLabel.text = self.movie?.originalTitle
        textInfoLabel.text = self.movie?.overview
        
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
