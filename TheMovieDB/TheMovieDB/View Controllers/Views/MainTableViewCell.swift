//
//  MainTableViewCell.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

protocol MovieSelectionDelegate {
    func didSelectMovie(movie: MovieModel)
    func didTapSeeAll(forListType type:MovieListType)
}

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var delegate: MovieSelectionDelegate!
    
    var listType: MovieListType!
    
    var networkManager: NetworkManager?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        networkManager = NetworkManager.shared()
        
        self.registerCells()
    }
    
    func registerCells() {
        let movieViewCell = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        self.collectionView.register(movieViewCell, forCellWithReuseIdentifier: "MovieCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func reloadData() {
        
        self.collectionView.reloadData()
        self.setNeedsDisplay()
    }
    
    var tapped = false
    @IBAction func seeAll_Tap(_ sender: Any) {
        if tapped {
            return
        }
        tapped = true
        self.delegate.didTapSeeAll(forListType: listType)
        tapped = false
    }
}

extension MainTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch listType! {
        case .topRated:
            return networkManager?.topRatedMovies.count ?? 0 < 20 ? networkManager?.topRatedMovies.count ?? 0 : 20
        case .popular:
            return networkManager?.popularMovies.count ?? 0 < 20 ? networkManager?.popularMovies.count ?? 0 : 20
        case .nowPlaying:
            return networkManager?.nowPlayingMovies.count ?? 0 < 20 ? networkManager?.nowPlayingMovies.count ?? 0 : 20
        case .upcoming:
            return networkManager?.upcomingMovies.count ?? 0 < 20 ? networkManager?.upcomingMovies.count ?? 0 : 20
        case .search:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
        
        switch listType! {
        case .topRated:
            cell.setup(content: networkManager?.topRatedMovies[indexPath.item])
        case .popular:
            cell.setup(content: networkManager?.popularMovies[indexPath.item])
        case .nowPlaying:
            cell.setup(content: networkManager?.nowPlayingMovies[indexPath.item])
        case .upcoming:
            cell.setup(content: networkManager?.upcomingMovies[indexPath.item])
        case .search:
            print("nothing to do here")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if tapped {
            return
        }
        tapped = true
        
        switch listType! {
        case .topRated:
            self.delegate.didSelectMovie(movie: (networkManager?.topRatedMovies[indexPath.item])!)
        case .popular:
            self.delegate.didSelectMovie(movie: (networkManager?.popularMovies[indexPath.item])!)
        case .nowPlaying:
            self.delegate.didSelectMovie(movie: (networkManager?.nowPlayingMovies[indexPath.item])!)
        case .upcoming:
            self.delegate.didSelectMovie(movie: (networkManager?.upcomingMovies[indexPath.item])!)
        default:
            print("Something wrong with the listType")
        }
        
        tapped = false
    }
    
}
