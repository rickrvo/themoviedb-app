//
//  WatchListViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

class WatchListViewController: UIViewController {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var noItemsView: UIView!
    @IBOutlet weak var noItemsLabel: UILabel!
    
    var networkManager : NetworkManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.networkManager = NetworkManager.shared()
        
        self.registerCells()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLists), name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.navView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.backButton.setImage(UIImage(named: "backWhite"), for: .normal)
            self.noItemsView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.noItemsLabel.textColor = UIColor.white
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.collectionView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            return .lightContent
        }
        return .default
    }
    
    func registerCells() {
        let movieViewCell = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        self.collectionView.register(movieViewCell, forCellWithReuseIdentifier: "MovieCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    @objc func updateLists() {
        self.collectionView.reloadData()
    }
    
    // MARK: - Navigation
    
    var tapped: Bool = false
    @IBAction func backButtonTap(_ sender: Any) {
        
        if tapped {
            return
        }
        tapped = true
        
        let nav = self.navigationController
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().popFromRight(), forKey: nil)
            nav?.popViewController(animated: false)
        }
        
        tapped = false
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    }
    
}

extension WatchListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let watch = RealmDataManager.fetchData(for: WatchListModelRealm.self) {
            if watch.count == 0 {
                self.noItemsView.isHidden = false
            } else {
                self.noItemsView.isHidden = true
            }
            return watch.count
        } else {
            self.noItemsView.isHidden = false
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
        
        if let watchs = RealmDataManager.fetchData(for: WatchListModelRealm.self) {
            
            let watchModel = WatchListMapper.convertRealmObjectToModel(response: watchs[indexPath.row])
            
            cell.setup(content: watchModel.movie)
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if tapped {
            return
        }
        tapped = true
        
        if let cell:MovieCollectionViewCell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell {
            
            if let movie = cell.movie {
                let viewController:MovieDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
                viewController.movie = movie
                self.navigationController?.show(viewController, sender: self)
            }
        }
        
        tapped = false
    }
    
    // Make Cells align in the center
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let cellWidth : CGFloat = 108.0
        
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        
        return UIEdgeInsets(top: 15, left: edgeInsets, bottom: 0, right: edgeInsets)
        
    }
    
}
