//
//  MovieDetailsViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit
import Cosmos

class MovieDetailsViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var movie:MovieModel!
    
    var networkManager : NetworkManager?
    var loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.networkManager = NetworkManager.shared()
        
        self.titleLabel.text = movie.originalTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.navView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.backButton.setImage(UIImage(named: "backWhite"), for: .normal)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            return .lightContent
        }
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.loadingView.showOverlayTransparent(over: self.view)
        
        self.networkManager?.getMovieDetails(for: movie, completion: { [unowned self] (movieResult) in
            if movieResult == nil {
                self.backButtonTap(self)
                return
            }
            print("reading movie details for: " + (self.movie.originalTitle ?? "") )
            if let movie = movieResult {
                self.movie = movie
                self.loadPageInfo()
            }
        })
    }
    
    func loadPageInfo() {
        
        self.loadingView.hideOverlayView()
        
        self.tableView.reloadData()
    }
    
    // MARK: - Navigation
    var tapped = false
    @IBAction func backButtonTap(_ sender: Any) {
        if tapped {
            return
        }
        tapped = false
        self.navigationController?.popViewController(animated: true)
        tapped = true
    }
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //    }
    
}

extension MovieDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 170
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 6
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            if let cell:MovieDetailHeaderCell = tableView.dequeueReusableCell(withIdentifier: "CellHeader") as? MovieDetailHeaderCell {
                cell.delegate = self
                cell.setup(content: movie)
                return cell
            }
        }
        return nil
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell:MovieDetailBackgroundImageCell = tableView.dequeueReusableCell(withIdentifier: "CellImage", for: indexPath) as? MovieDetailBackgroundImageCell {
                cell.setup(content: movie)
                return cell
            }
        } else {
            switch indexPath.row {
            case 0:
                if let cell:MovieDetailTextCell = tableView.dequeueReusableCell(withIdentifier: "CellText", for: indexPath) as? MovieDetailTextCell {
                    
                    cell.setup(content: movie)
                    return cell
                }
            case 1:
                if let cell:MovieDetailLineCell = tableView.dequeueReusableCell(withIdentifier: "CellLine", for: indexPath) as? MovieDetailLineCell {
                    
                    cell.setup(key: "RELEASE_DATE".localized, value: self.movie?.releaseDate)
                    return cell
                }
            case 2:
                if let cell:MovieDetailMultiLineCell = tableView.dequeueReusableCell(withIdentifier: "CellMultiLine", for: indexPath) as? MovieDetailMultiLineCell {
                    
                    var companies = ""
                    for c in self.movie?.genres ?? [] {
                        companies += (c.name ?? "") + "\n"
                    }
                    cell.setup(key: "GENRES".localized, value: companies)
                    
                    return cell
                }
            case 3:
                if let cell:MovieDetailLineCell = tableView.dequeueReusableCell(withIdentifier: "CellLine", for: indexPath) as? MovieDetailLineCell {
                    
                    cell.setup(key: "REVENUE".localized, value: String(self.movie?.revenue ?? 0).toDollar)
                    return cell
                }
            case 4:
                if let cell:MovieDetailMultiLineCell = tableView.dequeueReusableCell(withIdentifier: "CellMultiLine", for: indexPath) as? MovieDetailMultiLineCell {
                    
                    var companies = ""
                    for c in self.movie?.productionCompanies ?? [] {
                        companies += (c.name ?? "") + "\n"
                    }
                    cell.setup(key: "PRODUCTION_COMPANIES".localized, value: companies)
                    
                    return cell
                }
            case 5:
                if let cell:MovieDetailMultiLineCell = tableView.dequeueReusableCell(withIdentifier: "CellMultiLine", for: indexPath) as? MovieDetailMultiLineCell {
                    
                    var companies = ""
                    for c in self.movie?.productionCountries ?? [] {
                        companies += (c.name ?? "") + "\n"
                    }
                    cell.setup(key: "PRODUCTION_COUNTRIES".localized, value: companies)
                    
                    return cell
                }
            default:
                return UITableViewCell()
            }
            
        }
        return UITableViewCell()
    }
    
    
}

extension MovieDetailsViewController: MovieShareDelegate {
    
    func didTapShare() {
        let url = networkManager!.imageBaseURL + networkManager!.imageSize780 + (self.movie?.posterPath ?? "")
        URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil { return }
            
            DispatchQueue.main.async(execute: { () -> Void in
                if let image = UIImage(data: data!) {
                    let text = self.movie?.originalTitle
                    let url = URL(string: (self.movie?.homepage ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!
                    let avc = UIActivityViewController(activityItems: [image, url, text ?? ""], applicationActivities: nil)
                    avc.excludedActivityTypes = [ UIActivity.ActivityType.airDrop,
                                                  UIActivity.ActivityType.assignToContact,
                                                  UIActivity.ActivityType.openInIBooks,
                                                  UIActivity.ActivityType.addToReadingList ]
                    avc.popoverPresentationController?.sourceView = self.view
                    
                    
                    // present the view controller
                    self.navigationController?.present(avc, animated: true, completion: { () in
                        UINavigationBar.appearance().barTintColor = UIColor.white
                        
                        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor : UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)], for: .normal)
                        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor : UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)], for: .highlighted)
                        
                    })
                }
            })
            
        }).resume()
    }
    
}
