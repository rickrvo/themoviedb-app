//
//  ViewController.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import UIKit

enum ListType {
    case movies
    case favorites
}

class MainViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var listTypeSegment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var favoriteCollectionView: UICollectionView!
    
    @IBOutlet weak var noItemsView: UIView!
    @IBOutlet weak var noItemsLabel: UILabel!
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchField: UITextField!
    
    var networkManager : NetworkManager?
    
    let loadingView = LoadingView()
    
    var selectedSegment = ListType.movies
    var tapped: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        
        networkManager = NetworkManager.shared()
        
        let globalQueue = DispatchQueue.main
        globalQueue.async {
            self.loadMovies()
        }
        
        self.registerCells()
        self.registerSideMenu()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLists), name: NSNotification.Name("UpdateLists"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            self.backgroundView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.navView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.menuButton.setImage(UIImage(named: "MenuWhite"), for: .normal)
            self.searchButton.setImage(UIImage(named: "SearchWhite"), for: .normal)
            self.noItemsView.backgroundColor = UIColor(hex: env.backgroundColor)
            self.noItemsLabel.textColor = UIColor.white
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let env = EnvironmentSettings().getCurrentEnviroment()
        if env.name != "white" {
            return .lightContent
        }
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchField.placeholder = "SEARCH_PLACEHOLDER".localized
    }
    
    func registerCells() {
        let mainViewCell = UINib(nibName: "MainTableViewCell", bundle: nil)
        self.tableView.register(mainViewCell, forCellReuseIdentifier: "cell")
        
        let favoriteViewCell = UINib(nibName: "MovieCollectionViewCell", bundle: nil)
        self.favoriteCollectionView.register(favoriteViewCell, forCellWithReuseIdentifier: "MovieCell")
        self.favoriteCollectionView.delegate = self
        self.favoriteCollectionView.dataSource = self
    }
    
    func registerSideMenu() {
        NotificationCenter.default.addObserver(self, selector: #selector(openGenres), name: NSNotification.Name("SideMenu-Genres"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openWatchList), name: NSNotification.Name("SideMenu-WatchList"), object: nil)
    }
    
    @IBAction func listTypeSegmentChanged(_ sender: Any) {
        
        if self.listTypeSegment.selectedSegmentIndex == 0 {
            selectedSegment = .movies
            tableView.isHidden = false
            favoriteCollectionView.isHidden = true
            noItemsView.isHidden = true
        } else {
            selectedSegment = .favorites
            tableView.isHidden = true
            favoriteCollectionView.isHidden = false
            
            if let favorites = RealmDataManager.fetchData(for: FavoriteModelRealm.self) {
                if favorites.count == 0 {
                    self.noItemsView.isHidden = false
                } else {
                    self.noItemsView.isHidden = true
                }
            } else {
                self.noItemsView.isHidden = false
            }
        }
        self.tableView.reloadData()
        self.favoriteCollectionView.reloadData()
    }
    
    
    func loadMovies() {
        self.loadingView.showOverlayTransparent(over: self.view)
        var simultaniousLoads = 4
        
        networkManager?.getGenres(completion: nil)
        networkManager?.getMoviesList(type: .topRated, completion: { [unowned self] (success) in
            if success {
                self.tableView.reloadData()
            }
            simultaniousLoads -= 1
            if simultaniousLoads == 0 {
                self.loadingView.hideOverlayView()
            }
        })
        networkManager?.getMoviesList(type: .popular, completion: { [unowned self] (success) in
            if success {
                self.tableView.reloadData()
            }
            simultaniousLoads -= 1
            if simultaniousLoads == 0 {
                self.loadingView.hideOverlayView()
            }
        })
        networkManager?.getMoviesList(type: .nowPlaying, completion: { [unowned self] (success) in
            if success {
                self.tableView.reloadData()
            }
            simultaniousLoads -= 1
            if simultaniousLoads == 0 {
                self.loadingView.hideOverlayView()
            }
        })
        networkManager?.getMoviesList(type: .upcoming, completion: { [unowned self] (success) in
            if success {
                self.tableView.reloadData()
            }
            simultaniousLoads -= 1
            if simultaniousLoads == 0 {
                self.loadingView.hideOverlayView()
            }
        })
    }
    
    
    // MARK: - NotificationCenter Signals
    
    @objc func openGenres() {
        
        let viewController:GenresViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenresViewController") as! GenresViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(viewController, animated: false)
        }
    }
    
    @objc func openWatchList() {
        
        let viewController:WatchListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WatchListViewController") as! WatchListViewController
        let nav = self.navigationController
        
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(viewController, animated: false)
        }
    }
    
    @objc func updateLists() {
        
        self.tableView.reloadData()
        self.favoriteCollectionView.reloadData()
        
        if self.listTypeSegment.selectedSegmentIndex == 1 {
            
            if let favorites = RealmDataManager.fetchData(for: FavoriteModelRealm.self) {
                if favorites.count == 0 {
                    self.noItemsView.isHidden = false
                } else {
                    self.noItemsView.isHidden = true
                }
            } else {
                self.noItemsView.isHidden = false
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func menuButtonTap(_ sender: Any) {
        
         NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
    }
    
    @IBAction func genreButtonTap(_ sender: Any) {
        
        let viewController:GenresViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GenresViewController") as! GenresViewController
        let nav = self.navigationController
        DispatchQueue.main.async {
            nav?.view.layer.add(CATransition().segueFromLeft(), forKey: nil)
            nav?.pushViewController(viewController, animated: false)
        }
    }
    
    @IBAction func searchButtonTap(_ sender: Any) {
        
        if tapped {
            return
        }
        tapped = true
        
        if self.searchField.isHidden {
            self.searchField.alpha = 0.0
            self.searchField.isHidden = false
            self.searchField.becomeFirstResponder()
            UIView.animate(withDuration: 0.3, animations: {
                self.searchField.alpha = 1.0
            }) { (finished) in
                if finished {
                    self.searchField.becomeFirstResponder()
                }
            }
        } else {
            self.view.endEditing(true)
            self.searchField.alpha = 1.0
            UIView.animate(withDuration: 0.3, animations: {
                self.searchField.alpha = 0.0
            }) { (finished) in
                if finished {
                    self.searchField.isHidden = true
                }
            }
        }
        tapped = false
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 && networkManager?.topRatedMovies.count ?? 0 > 0 {
            return 1
        }
        if section == 1 && networkManager?.popularMovies.count ?? 0 > 0 {
            return 1
        }
        if section == 2 && networkManager?.nowPlayingMovies.count ?? 0 > 0 {
            return 1
        }
        if section == 3 && networkManager?.upcomingMovies.count ?? 0 > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MainTableViewCell
        cell.delegate = self
        
        switch indexPath.section {
        case 0:
            cell.labelTitle.text = "TOP_RATED".localized
            cell.listType = .topRated
        case 1:
            cell.labelTitle.text = "MOST_POPULAR".localized
            cell.listType = .popular
        case 2:
            cell.labelTitle.text = "NOW_PLAYING".localized
            cell.listType = .nowPlaying
        case 3:
            cell.labelTitle.text = "UPCOMING".localized
            cell.listType = .upcoming
        default:
            print("section out of bounds")
        }
        cell.reloadData()
        
        return cell
    }
    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let favorites = RealmDataManager.fetchData(for: FavoriteModelRealm.self) {
            return favorites.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
        
        if let favorites = RealmDataManager.fetchData(for: FavoriteModelRealm.self) {
            
            let favModel = FavoriteMapper.convertRealmObjectToModel(response: favorites[indexPath.row])
            
            cell.setup(content: favModel.movie)
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let cellWidth : CGFloat = 108.0
        
        let numberOfCells = floor(self.view.frame.size.width / cellWidth)
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1)
        
        return UIEdgeInsets(top: 15, left: edgeInsets, bottom: 0, right: edgeInsets)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if tapped {
            return
        }
        tapped = true
        
        if let cell:MovieCollectionViewCell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell {
            
            if let movie = cell.movie {
                let viewController:MovieDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
                viewController.movie = movie
                self.navigationController?.show(viewController, sender: self)
            }
        }
        
        tapped = false
    }
    
}


extension MainViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let search = self.searchField.text, search != "" {
            let viewController:MovieListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
            viewController.listType = .search
            viewController.searchTerm = search
            self.navigationController?.show(viewController, sender: self)
        }
        self.view.endEditing(true)
        return true
    }
}

extension MainViewController: MovieSelectionDelegate {
    
    func didSelectMovie(movie: MovieModel) {
        let viewController:MovieDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        viewController.movie = movie
        self.navigationController?.show(viewController, sender: self)
    }
    
    func didTapSeeAll(forListType type: MovieListType) {
        let viewController:MovieListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
        viewController.listType = type
        self.navigationController?.show(viewController, sender: self)
    }
}

// MARK: - Navigation
//extension ViewController {
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destination = (segue.destination as? GenresViewController) {
//            destination. = someImportantData
//        }
//    }
//}


