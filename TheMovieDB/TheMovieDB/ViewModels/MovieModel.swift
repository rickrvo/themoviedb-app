//
//  MovieModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

open class MovieModel: Codable {
    
    var description: String? = nil
    var favoriteCount: Int? = nil
    var itemCount: Int? = nil
    var iso_639_1: String? = nil
    var name: String? = nil
    var posterPath: String? = nil
    var id: Int? = nil
    
    var adult: Bool? = nil
    var overview: String? = nil
    var popularity: Double? = nil
    var backdropPath: String? = nil
    var voteAverage: Double? = nil
    var originalLanguage: String? = nil
    var voteCount: Double? = nil
    var genres: [GenreModel]? = nil
    
    var title: String? = nil
    var video: Bool? = nil
    var releaseDate: String? = nil
    var originalTitle: String? = nil
    var genreIds: [Int]? = nil
    
    var belongsToCollection: MovieCollection? = nil
    var budget: Int? = nil
    var homepage: String? = nil
    var imdb_id: String? = nil
    var productionCompanies: [Keywords]? = nil
    var productionCountries: [Keywords]? = nil
    var revenue : Int? = nil
    var runtime: Int? = nil
    var spokenLanguages: [SpokenLanguages]? = nil
    var status: String? = nil
    var tagline: String? = nil
    
    init() {
        description = nil
        favoriteCount = nil
        itemCount = nil
        iso_639_1 = nil
        name = nil
        posterPath = nil
        id = nil
        
        adult = nil
        overview = nil
        popularity = nil
        backdropPath = nil
        voteAverage = nil
        originalLanguage = nil
        voteCount = nil
        genres = nil
        
        title = nil
        video = nil
        releaseDate = nil
        originalTitle = nil
        genreIds = nil
        belongsToCollection = nil
        budget = nil
        homepage = nil
        imdb_id = nil
        productionCompanies = nil
        productionCountries = nil
        revenue = nil
        runtime = nil
        spokenLanguages = nil
        status = nil
        tagline = nil
    }
}

extension MovieModel {
    enum CodingKeys: String, CodingKey {
        
        case  description
        case  favoriteCount = "favorite_count"
        case  itemCount = "item_count"
        case  iso_639_1
        case  name
        case  posterPath = "poster_path"
        case  id
        
        case  adult
        case  overview
        case  popularity
        case  backdropPath = "backdrop_path"
        case  voteAverage = "vote_average"
        case  originalLanguage = "original_language"
        case  voteCount = "vote_count"
        case  genreIds = "genre_ids"
        
        case  title
        case  video
        case  releaseDate = "release_date"
        case  originalTitle = "original_title"
        case  genres
        
        case  belongsToCollection = "belongs_to_collection"
        case  budget
        case  homepage
        case  imdb_id
        case  productionCompanies = "production_companies"
        case  productionCountries = "production_countries"
        case  revenue
        case  runtime
        case  spokenLanguages = "spoken_languages"
        case  status
        case  tagline
    }
}

public struct MovieCollection: Codable {
    public var id: Int?
    public var name: String?
    public var poster_path: String?
    public var backdrop_path: String?
}

public struct Keywords: Codable {
    var id: Int?
    var name: String?
}

public struct SpokenLanguages: Codable {
    var iso_639_1: String?
    var name: String?
}
