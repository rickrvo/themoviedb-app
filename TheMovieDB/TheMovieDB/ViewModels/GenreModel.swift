//
//  GenreModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

struct GenreModel: Codable {
    var id: Int? = nil
    var name: String? = nil
}

extension GenreModel  {
    enum CodingKeys: String, CodingKey {
        case  id
        case  name
    }
}
