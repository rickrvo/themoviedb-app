//
//  FavoriteModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 17/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

struct FavoriteModel: Codable {
    var id: Int? = nil
    var movie: MovieModel? = nil
}

extension FavoriteModel  {
    enum CodingKeys: String, CodingKey {
        case  id
        case  movie
    }
}
