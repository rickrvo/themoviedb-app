//
//  WatchListModel.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 19/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

struct WatchListModel: Codable {
    var id: Int? = nil
    var movie: MovieModel? = nil
}

extension WatchListModel  {
    enum CodingKeys: String, CodingKey {
        case  id
        case  movie
    }
}
