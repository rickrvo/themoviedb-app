//
//  Strings+.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 14/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    var toDollar: String {
        return "$ " + self
    }
    
    // MARK: -   From now on these is extra for this project, but it's here if we ever have to use it
    
    /// Returns date in ISO 8601 format
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
    
    /// Return MD5 of the string
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }
    
    /// Matches regex expression
    func matches(regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression) != nil
    }
    
    /// Trimming string of spaces
    func trimming() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    /// If string is blank
    var isNullOrEmptyOrNone: Bool {
        if (self.isEmpty || self.trimming().count == 0) {
            return true;
        }
        return false;
    }
    
    /// converts all types of String to Bool
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "YES", "yes", "1":
            return true
        case "False", "false", "NO", "no", "0":
            return false
        default:
            return nil
        }
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}
