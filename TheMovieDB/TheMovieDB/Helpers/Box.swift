//
//  Box.swift
//  TheMovieDB
//
//  Created by Henrique Ormonde on 15/09/19.
//  Copyright © 2019 Rick. All rights reserved.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value:T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}

// declaration
//var something: Box<String?> = Box("")

//something.value = "some value"

// usage on VC
//viewdidLoad()
//something.bind {
//  lable.text = $0
//}
